# nfhopdeny: netfilter queue listener to deny known trace hops.
# Copyright (C) 2021, 2024  Adonay Felipe Nogueira <https://libreplanet.org/wiki/User:Adfeno> <adfeno.7046@gmail.com>
#
# This file is part of nfhopdeny.
#
# nfhopdeny is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# nfhopdeny is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with nfhopdeny.  If not, see <https://www.gnu.org/licenses/>.

PROJECT_NAME = nfhopdeny
PROJECT_VERSION = 1

SHELL = /bin/sh
RM = rm -f
INSTALL = install
INSTALL_PROGRAM = $(INSTALL)
INSTALL_DATA = $(INSTALL) -m 644
MAKEINFO = makeinfo

prefix = /usr/local
exec_prefix = $(prefix)
datarootdir = $(prefix)/share
datadir = $(datarootdir)

sbindir = $(exec_prefix)/sbin
libdir = $(exec_prefix)/lib
sysconfdir = $(prefix)/etc
docdir = $(datarootdir)/doc/$(PROJECT_NAME)
infodir = $(datarootdir)/info

systemdsystemunitdir = $(libdir)/systemd/system
projectdatadir = $(datadir)/$(PROJECT_NAME)

all: FORCE
	@echo $(PROJECT_NAME) "is interpreted, not built. Simply install it to use."

$(PROJECT_NAME).info: $(PROJECT_NAME).texi fdl.texi
	$(MAKEINFO) $(srcdir)$(PROJECT_NAME).texi

info: FORCE $(PROJECT_NAME).info

installdirs: FORCE build-aux/mkinstalldirs
	"$(srcdir)build-aux/mkinstalldirs" \
		"$(DESTDIR)$(sbindir)" \
		"$(DESTDIR)$(systemdsystemunitdir)" \
		"$(DESTDIR)$(sysconfdir)" \
		"$(DESTDIR)$(projectdatadir)" \
		"$(DESTDIR)$(docdir)" \
		"$(DESTDIR)$(infodir)"

install: FORCE info installdirs \
		README \
		COPYING \
		$(PROJECT_NAME) \
		$(addprefix $(PROJECT_NAME).,service cfg info)
	$(NORMAL_INSTALL)
	$(INSTALL_PROGRAM) \
		"$(srcdir)$(PROJECT_NAME)" \
		"$(DESTDIR)$(sbindir)"
	$(INSTALL_DATA) \
		"$(srcdir)$(PROJECT_NAME).service" \
		"$(DESTDIR)$(systemdsystemunitdir)"
	$(INSTALL_DATA) \
		"$(srcdir)$(PROJECT_NAME).cfg" \
		"$(DESTDIR)$(sysconfdir)"
	$(INSTALL_DATA) \
		"$(srcdir)$(PROJECT_NAME).cfg" \
		"$(DESTDIR)$(projectdatadir)"
	$(INSTALL_DATA) \
		"$(srcdir)README" \
		"$(srcdir)COPYING" \
		"$(DESTDIR)$(docdir)"
	if test -f "$(PROJECT_NAME).info"; then \
		d="."; \
	else d="$(srcdir)"; \
	fi; \
	$(INSTALL_DATA) \
		"$$d/$(PROJECT_NAME).info" \
		"$(DESTDIR)$(infodir)"
	$(POST_INSTALL)
	if $(SHELL) -c 'install-info --version' > "/dev/null" 2>&1; then \
		install-info \
			--dir-file="$(DESTDIR)$(infodir)/dir" \
			"$(DESTDIR)$(infodir)/$(PROJECT_NAME).info"; \
	else true; \
	fi

clean: FORCE
	if test -f "$(PROJECT_NAME).info"; then \
		d="."; \
	else d="$(srcdir)"; \
	fi; \
	$(RM) "$$d/$(PROJECT_NAME).info"

uninstall: FORCE
	$(PRE_UNINSTALL)
	if $(SHELL) -c 'install-info --version' > "/dev/null" 2>&1; then \
		install-info --delete \
			--dir-file="$(DESTDIR)$(infodir)/dir" \
			"$(DESTDIR)$(infodir)/$(PROJECT_NAME).info"; \
	else true; \
	fi
	$(NORMAL_UNINSTALL)
	$(RM) \
		"$(DESTDIR)$(sbindir)/$(PROJECT_NAME)" \
		"$(DESTDIR)$(systemdsystemunitdir)/$(PROJECT_NAME).service" \
		"$(DESTDIR)$(sysconfdir)/$(PROJECT_NAME).cfg" \
		"$(DESTDIR)$(infodir)/$(PROJECT_NAME).info"
	rm -fr \
		"$(DESTDIR)$(projectdatadir)" \
		"$(DESTDIR)$(docdir)"

FORCE:
